<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
$bootstrap = __DIR__ . '/../tests_run/app/bootstrap.php';

if (!file_exists($bootstrap)) {
    echo "Requirements not installed.\nYou'll need to run 'composer install' before running the tests.\n";
    exit(1);
}

require_once $bootstrap;