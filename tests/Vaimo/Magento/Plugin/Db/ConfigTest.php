<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace tests\Vaimo\Magento\Plugin\Db;

class ConfigTest extends \PHPUnit\Framework\TestCase
{
    public function testGetConfigWithoutHost()
    {
        $config = new \Vaimo\Magento\Plugin\Db\Config([
            'user' => 'user',
            'password' => 'password'
        ]);

        $expected = [
            'active' => true,
            'dbname' => 'mysql',
            'username' => 'user',
            'password' => 'password',
            'host' => 'localhost'
        ];

        $this->assertEquals($expected, $config->getConfig());
    }

    public function testGetConfigWithHost()
    {
        $config = new \Vaimo\Magento\Plugin\Db\Config([
            'user' => 'user',
            'password' => 'password',
            'host' => 'host'
        ]);

        $expected = [
            'active' => true,
            'dbname' => 'mysql',
            'username' => 'user',
            'password' => 'password',
            'host' => 'host'
        ];

        $this->assertEquals($expected, $config->getConfig());
    }

    public function testGetUser()
    {
        $config = new \Vaimo\Magento\Plugin\Db\Config([
            'user' => 'test_user'
        ]);

        $this->assertEquals('test_user', $config->getUser());
    }

    public function testGetPassword()
    {
        $config = new \Vaimo\Magento\Plugin\Db\Config([
            'password' => 'test_password'
        ]);

        $this->assertEquals('test_password', $config->getPassword());
    }

    public function testGetHost()
    {
        $config = new \Vaimo\Magento\Plugin\Db\Config([
            'host' => 'test_host'
        ]);

        $this->assertEquals('test_host', $config->getHost());
    }

    public function testGetHostLocalhostFallback()
    {
        $config = new \Vaimo\Magento\Plugin\Db\Config([]);

        $this->assertEquals('localhost', $config->getHost());
    }
}
