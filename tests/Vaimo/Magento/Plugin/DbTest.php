<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace tests\Vaimo\Magento\Plugin;

class DbTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Vaimo\Magento\Plugin\Db
     */
    protected $db;

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    protected $connection;

    protected function setUp(): void
    {
        $this->connection = $this->getMockBuilder('Magento\Framework\DB\Adapter\Pdo\Mysql')
            ->disableOriginalConstructor()
            ->setMethods(['query'])
            ->getMock();

        $this->db = new \Vaimo\Magento\Plugin\Db($this->connection);

    }

    public function testCreateDatabaseQuery()
    {
        $this->connection->expects($this->once())
            ->method('query')
            ->with(
                'CREATE DATABASE IF NOT EXISTS `test`'
            );

        $this->db->createDatabase('test');
    }

    public function testDatabaseExistsQuery()
    {
        $result = $this->getMockForAbstractClass('Zend_Db_Statement_Interface');

        $this->connection->expects($this->once())
            ->method('query')
            ->with("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = ?", ['test'])
            ->willReturn($result);

        $this->db->databaseExists('test');
    }

    public function testDatabaseExistsShouldReturnTrueIfRowsCountEquals1()
    {
        $result = $this->getMockForAbstractClass(
            'Zend_Db_Statement_Interface',
            ['rowCount'],
            callOriginalConstructor: false
        );

        $result->expects($this->once())
            ->method('rowCount')
            ->willReturn(1);

        $this->connection->expects($this->once())
            ->method('query')
            ->willReturn($result);

        $this->assertTrue($this->db->databaseExists('test'));
    }

    public function testDatabaseExistsShouldReturnFalseIfRowsCountNotEquals1()
    {
        $result = $this->getMockForAbstractClass(
            'Zend_Db_Statement_Interface',
            ['rowCount'],
            callOriginalConstructor: false
        );

        $result->expects($this->once())
            ->method('rowCount')
            ->willReturn(0);

        $this->connection->expects($this->once())
            ->method('query')
            ->willReturn($result);

        $this->assertFalse($this->db->databaseExists('test'));
    }

    public function testDropDatabaseQuery()
    {
        $this->connection->expects($this->once())
            ->method('query')
            ->with('DROP DATABASE `test`');

        $this->db->dropDatabase('test');
    }
}
