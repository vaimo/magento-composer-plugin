<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace tests\Vaimo\Magento\Plugin\Installer;

class DataTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Vaimo\Magento\Plugin\Installer\Data
     */
    protected $installerData;

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    protected $config;

    protected $dbConfig;

    protected $host;

    protected function setUp(): void
    {
        $this->config = $this->getMockBuilder('Vaimo\Magento\Plugin\Config')
            ->disableOriginalConstructor()
            ->setMethods(['getConfig'])
            ->getMock();

        $this->config->expects($this->any())
            ->method('getConfig')
            ->willReturnCallback(function($key, $default) {
                return $default;
            });

        $this->dbConfig = $this->getMockBuilder('Vaimo\Magento\Plugin\Db\Config')
            ->disableOriginalConstructor()
            ->setMethods(['getUser', 'getPassword', 'getHost'])
            ->getMock();

        $this->dbConfig->expects($this->any())
            ->method('getUser')
            ->willReturn('user');

        $this->dbConfig->expects($this->any())
            ->method('getPassword')
            ->willReturn('password');

        $this->host = $this->getMockBuilder('Vaimo\Magento\Plugin\Host')
            ->setMethods(['getFQDN'])
            ->getMock();

        $this->host->expects($this->any())
            ->method('getFQDN')
            ->willReturn('test.dev');

        $this->installerData = new \Vaimo\Magento\Plugin\Installer\Data(
            'vendor/test_name',
            '/var/www/site',
            $this->config,
            $this->dbConfig,
            $this->host
        );
    }

    public function testGetRequest()
    {
        $expected = [
            'base-url' => 'http://test.dev/site/',
            'backend-frontname' => 'admin',
            'session-save' => 'files',
            'db-host' => 'host',
            'db-name' => 'site',
            'db-user' => 'user',
            'db-password' => 'password',
            'db-model' => 'mysql4',
            'db-engine' => 'innodb',
            'db-prefix' => '',
            'db-ssl-key' => '',
            'db-ssl-cert' => '',
            'db-ssl-ca' => '',
            'db-ssl-verify' => false,
            'db-init-statements' => 'SET NAMES utf8;',
            'admin-firstname' => 'Magento',
            'admin-lastname' => 'User',
            'admin-email' => 'user@example.com',
            'admin-user' => 'admin',
            'admin-password' => 'test123',
            'language' => 'en_US',
            'currency' => 'USD',
            'timezone' => 'America/Chicago',
            'use-rewrites' => true,
            'cleanup-database' => false
        ];
        $this->dbConfig->expects($this->any())
            ->method('getHost')
            ->willReturn('host');

        $this->assertEquals($expected, $this->installerData->getRequest());
    }

    public function testGetBaseUrl()
    {
        $this->assertEquals('http://test.dev/site/', $this->installerData->getBaseUrl());
    }

    public function testGetBaseUrlFromConfig()
    {
        $this->config->expects($this->at(0))
            ->method('getConfig')
            ->with('fqdn', 'test.dev');

        $this->config->expects($this->at(1))
            ->method('getConfig')
            ->with('base-url', 'http://test.dev/site/');

        $this->installerData->getBaseUrl();
    }

    public function testGetDbHost()
    {
        $this->dbConfig->expects($this->any())
            ->method('getHost')
            ->willReturn('test-host');

        $this->assertEquals('test-host', $this->installerData->getDbHost());
    }

    public function testGetDbHostFromConfig()
    {
        $this->dbConfig->expects($this->any())
            ->method('getHost')
            ->willReturn('test-host');

        $this->config->expects($this->once())
            ->method('getConfig')
            ->with('db-host', 'test-host');

        $this->installerData->getDbHost();
    }

    public function testGetDbName()
    {
        $this->assertEquals('site', $this->installerData->getDbName());
    }

    public function testGetDbNameFromConfig()
    {
        $this->config->expects($this->once())
            ->method('getConfig')
            ->with('db-name', 'site');

        $this->installerData->getDbName();
    }

    public function testGetDbUser()
    {
        $this->assertEquals('user', $this->installerData->getDbUser());
    }

    public function testGetDbPassword()
    {
        $this->assertEquals('password', $this->installerData->getDbPassword());
    }

    public function getBackendFrontname()
    {
        $this->assertEquals('admin', $this->installerData->getBackendFrontname());
    }

    public function testGetAdminUrl()
    {
        $this->assertEquals('http://test.dev/site/admin', $this->installerData->getAdminUrl());
    }

    public function testGetAdminUser()
    {
        $this->assertEquals('admin', $this->installerData->getAdminUser());
    }

    public function testGetAdminPassword()
    {
        $this->assertEquals('test123', $this->installerData->getAdminPassword());
    }
}
