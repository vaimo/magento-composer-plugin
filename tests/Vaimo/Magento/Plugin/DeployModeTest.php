<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace tests\Vaimo\Magento\Plugin;

class DeployModeTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManager;

    protected $mode;

    /**
     * @var \Vaimo\Magento\Plugin\DeployMode
     */
    protected $deployMode;

    protected function setUp(): void
    {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->mode = $this->getMockBuilder('Magento\Deploy\Model\Mode')
            ->disableOriginalConstructor()
            ->setMethods(['enableDeveloperMode', 'enableProductionMode'])
            ->getMock();

        $this->deployMode = $this->objectManager->getObject('Vaimo\Magento\Plugin\DeployMode', [
            'mode' => $this->mode
        ]);
    }

    public function testEnableModeShouldEnableDeveloperMode()
    {
        $this->mode->expects($this->once())
            ->method('enableDeveloperMode')
            ->with();

        $this->deployMode->enableMode('developer');
    }

    public function testEnableModeShouldEnableProductionMode()
    {
        $this->mode->expects($this->once())
            ->method('enableProductionMode')
            ->with();

        $this->deployMode->enableMode('production');
    }

    public function testEnableModeShouldNotEnableAnythingIfModeIsNull()
    {
        $this->mode->expects($this->never())
            ->method('enableDeveloperMode');

        $this->mode->expects($this->never())
            ->method('enableProductionMode');

        $this->deployMode->enableMode(null);
    }
}
