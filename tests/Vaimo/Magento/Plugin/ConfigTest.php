<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace tests\Vaimo\Magento\Plugin;

class ConfigTest extends \PHPUnit\Framework\TestCase
{
    public function testGetConfigShouldReturnEnvConfigIfExists()
    {
        $env = ['MCP_CONFIG1' => 'value_from_env'];
        $extra = ['config1' => 'value_from_extra'];

        $config = new \Vaimo\Magento\Plugin\Config($env, $extra);

        $this->assertEquals('value_from_env', $config->getConfig('config1'));
    }

    public function testGetConfigShouldReturnEnvConfigIfKeyContainsDash()
    {
        $env = ['MCP_CONFIG_1' => 'value_from_env'];
        $extra = ['config-1' => 'value_from_extra'];

        $config = new \Vaimo\Magento\Plugin\Config($env, $extra);

        $this->assertEquals('value_from_env', $config->getConfig('config-1'));
    }

    public function testGetConfigShouldReturnExtraConfigIfExists()
    {
        $env = ['MCP_CONFIG_1' => 'value_from_env'];
        $extra = ['config2' => 'value_from_extra'];

        $config = new \Vaimo\Magento\Plugin\Config($env, $extra);

        $this->assertEquals('value_from_extra', $config->getConfig('config2'));
    }

    public function testGetConfigShouldReturnExtraConfigIfKeyContainsDash()
    {
        $env = ['MCP_CONFIG_1' => 'value_from_env'];
        $extra = ['config-2' => 'value_from_extra'];

        $config = new \Vaimo\Magento\Plugin\Config($env, $extra);

        $this->assertEquals('value_from_extra', $config->getConfig('config-2'));
    }

    public function testGetConfigShouldReturnDefault()
    {
        $env = [];
        $extra = [];

        $config = new \Vaimo\Magento\Plugin\Config($env, $extra);

        $this->assertEquals('default_value', $config->getConfig('config1', 'default_value'));
    }

    public function testGetBooleanConfigString()
    {
        $env = ['MCP_CONFIG_1' => 'true'];

        $config = new \Vaimo\Magento\Plugin\Config($env);

        $this->assertTrue($config->getBooleanConfig('config-1'));
    }

    public function testGetBooleanConfigInt()
    {
        $env = ['MCP_CONFIG_1' => '1'];

        $config = new \Vaimo\Magento\Plugin\Config($env);

        $this->assertTrue($config->getBooleanConfig('config-1'));
    }

    public function testGetBooleanShouldReturnNullIfConfigNotExists()
    {
        $env = ['MCP_CONFIG_1' => '1'];

        $config = new \Vaimo\Magento\Plugin\Config($env);

        $this->assertNull($config->getBooleanConfig('config-2'));
    }
}
