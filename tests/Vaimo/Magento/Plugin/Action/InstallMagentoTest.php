<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace tests\Vaimo\Magento\Plugin\Action;

class InstallMagentoTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManager;

    protected $installerData;

    protected $config;

    protected $db;

    protected $io;

    protected $deployMode;

    /**
     * @var \Vaimo\Magento\Plugin\Action\InstallMagento
     */
    protected $action;

    protected function setUp(): void
    {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->installerData = $this->getMockBuilder('\Vaimo\Magento\Plugin\Installer\Data')
            ->disableOriginalConstructor()
            ->setMethods(['getRequest', 'getBaseUrl', 'getAdminUrl', 'getDbHost', 'getDbName', 'getDbUser',
                'getDbPassword', 'getAdminUser', 'getAdminPassword', 'shouldCleanupDatabase'])
            ->getMock();

        $this->installerData->expects($this->any())
            ->method('getBaseUrl')
            ->willReturn('http://testurl.com');

        $this->installerData->expects($this->any())
            ->method('getAdminUrl')
            ->willReturn('http://testurl.com/admin');

        $this->installerData->expects($this->any())
            ->method('getDbHost')
            ->willReturn('localhost');

        $this->installerData->expects($this->any())
            ->method('getDbName')
            ->willReturn('databasename');

        $this->installerData->expects($this->any())
            ->method('getDbUser')
            ->willReturn('user');

        $this->installerData->expects($this->any())
            ->method('getDbPassword')
            ->willReturn('password');

        $this->installerData->expects($this->any())
            ->method('getAdminUser')
            ->willReturn('admin');

        $this->installerData->expects($this->any())
            ->method('getAdminPassword')
            ->willReturn('test123');

        $this->db = $this->getMockBuilder('Vaimo\Magento\Plugin\Db')
            ->disableOriginalConstructor()
            ->setMethods(['createDatabase', 'databaseExists', 'dropDatabase'])
            ->getMock();

        $this->config = $this->getMockBuilder('Vaimo\Magento\Plugin\Config')
            ->disableOriginalConstructor()
            ->setMethods(['getBooleanConfig'])
            ->getMock();

        $this->config->expects($this->any())
            ->method('getBooleanConfig')
            ->willReturn(true);

        $this->io = $this->getMockForAbstractClass(
            'Composer\IO\IOInterface',
            ['isVerbose', 'write', 'askConfirmation', 'isInteractive'],
            callOriginalConstructor: false
        );

        $this->io->expects($this->any())
            ->method('isInteractive')
            ->willReturn(true);

        $this->deployMode = $this->getMockBuilder('Vaimo\Magento\Plugin\DeployMode')
            ->disableOriginalConstructor()
            ->setMethods(['enableMode'])
            ->getMock();
    }

    public function testExecuteShouldReturnFalseIfMagentoAlreadyInstalled()
    {
        $magento = $this->getMockBuilder('Vaimo\Magento\Plugin\Magento')
            ->disableOriginalConstructor()
            ->setMethods(['isInstalled'])
            ->getMock();

        $magento->expects($this->once())
            ->method('isInstalled')
            ->willReturn(true);

        $arguments = [
            'config' => $this->config,
            'magento' => $magento
        ];

        $this->action = $this->objectManager->getObject('Vaimo\Magento\Plugin\Action\InstallMagento', $arguments);

        $this->assertFalse($this->action->execute($this->installerData));
    }

    public function testExecuteShouldOuputMessageIfVerboseAndMagentoAlreadyInstalled()
    {
        $magento = $this->getMockBuilder('Vaimo\Magento\Plugin\Magento')
            ->disableOriginalConstructor()
            ->setMethods(['isInstalled'])
            ->getMock();

        $magento->expects($this->once())
            ->method('isInstalled')
            ->willReturn(true);

        $this->io->expects($this->once())
            ->method('isVerbose')
            ->willReturn(true);

        $this->io->expects($this->once())
            ->method('write')
            ->with('<info>Magento site already installed</info>');

        $arguments = [
            'config' => $this->config,
            'io' => $this->io,
            'magento' => $magento
        ];

        $this->action = $this->objectManager->getObject('Vaimo\Magento\Plugin\Action\InstallMagento', $arguments);

        $this->assertFalse($this->action->execute($this->installerData));
    }

    public function testExecuteShouldInstallMagento()
    {
        $installer = $this->getMockBuilder('Magento\Setup\Model\Installer')
            ->disableOriginalConstructor()
            ->setMethods(['install'])
            ->getMock();

        $installer->expects($this->once())
            ->method('install');

        $arguments = [
            'config' => $this->config,
            'installer' => $installer
        ];

        $this->action = $this->objectManager->getObject('Vaimo\Magento\Plugin\Action\InstallMagento', $arguments);
        $this->action->execute($this->installerData);
    }

    public function testExecuteShouldCreateDatabase()
    {
        $this->db->expects($this->once())
            ->method('createDatabase')
            ->with('databasename');

        $arguments = [
            'config' => $this->config,
            'db' => $this->db
        ];

        $this->action = $this->objectManager->getObject('Vaimo\Magento\Plugin\Action\InstallMagento', $arguments);
        $this->action->execute($this->installerData);
    }

    public function testExecuteShouldNotDropAndCreateDatabaseIfCleanupDatabaseConfigIsSet()
    {
        $this->installerData->expects($this->any())
            ->method('shouldCleanupDatabase')
            ->willReturn(true);

        $this->db->expects($this->never())
            ->method('dropDatabase');

        $this->db->expects($this->never())
            ->method('createDatabase');

        $arguments = [
            'config' => $this->config,
            'db' => $this->db
        ];

        $this->action = $this->objectManager->getObject('Vaimo\Magento\Plugin\Action\InstallMagento', $arguments);
        $this->action->execute($this->installerData);
    }

    public function testExecuteShouldAskUserIfDatabaseAlreadyExists()
    {
        $this->db->expects($this->once())
            ->method('databaseExists')
            ->willReturn(true);

        $this->io->expects($this->once())
            ->method('askConfirmation')
            ->with('A database named databasename already exists, do you want to replace it? [y/N] ', false);

        $arguments = [
            'io' => $this->io,
            'config' => $this->config,
            'db' => $this->db
        ];

        $this->action = $this->objectManager->getObject('Vaimo\Magento\Plugin\Action\InstallMagento', $arguments);
        $this->action->execute($this->installerData);
    }

    public function testExecuteShouldDropDatabaseIfUserAnswerYes()
    {
        $this->io->expects($this->once())
            ->method('askConfirmation')
            ->willReturn(true);

        $this->db->expects($this->once())
            ->method('databaseExists')
            ->willReturn(true);

        $this->db->expects($this->once())
            ->method('dropDatabase')
            ->with('databasename');

        $arguments = [
            'io' => $this->io,
            'config' => $this->config,
            'db' => $this->db
        ];

        $this->action = $this->objectManager->getObject('Vaimo\Magento\Plugin\Action\InstallMagento', $arguments);
        $this->action->execute($this->installerData);
    }

    public function testExecuteShouldNotAskUserIfNoInteraction()
    {
        $this->db->expects($this->once())
            ->method('databaseExists')
            ->willReturn(true);

        $this->io = $this->getMockForAbstractClass(
            'Composer\IO\IOInterface',
            ['askConfirmation', 'isInteractive'],
            callOriginalConstructor: false
        );

        $this->io->expects($this->once())
            ->method('isInteractive')
            ->willReturn(false);

        $this->io->expects($this->never())
            ->method('askConfirmation');

        $arguments = [
            'io' => $this->io,
            'config' => $this->config,
            'db' => $this->db
        ];

        $this->action = $this->objectManager->getObject('Vaimo\Magento\Plugin\Action\InstallMagento', $arguments);
        $this->action->execute($this->installerData);
    }

    public function testExecuteShouldOutputInstallerInfoWhenDone()
    {
        $this->io->expects($this->once())
            ->method('write')
            ->with("<info>Main url {$this->installerData->getBaseUrl()}
Admin url {$this->installerData->getAdminUrl()}
User: {$this->installerData->getAdminUser()}
Password: {$this->installerData->getAdminPassword()}</info>");

        $arguments = [
            'config' => $this->config,
            'io' => $this->io
        ];

        $this->action = $this->objectManager->getObject('Vaimo\Magento\Plugin\Action\InstallMagento', $arguments);
        $this->action->execute($this->installerData);
    }

    public function testExecuteShouldReloadMagentoAfterInstallation()
    {
        $magento = $this->getMockBuilder('Vaimo\Magento\Plugin\Magento')
            ->disableOriginalConstructor()
            ->setMethods(['isInstalled', 'reload'])
            ->getMock();

        $magento->expects($this->once())
            ->method('isInstalled')
            ->willReturn(false);

        $magento->expects($this->once())
            ->method('reload');

        $arguments = [
            'config' => $this->config,
            'magento' => $magento
        ];

        $this->action = $this->objectManager->getObject('Vaimo\Magento\Plugin\Action\InstallMagento', $arguments);
        $this->action->execute($this->installerData);
    }

    public function testExecuteShouldSetDeployMode()
    {
        $config = $this->getMockBuilder('Vaimo\Magento\Plugin\Config')
            ->disableOriginalConstructor()
            ->setMethods(['getConfig'])
            ->getMock();

        $config->expects($this->once())
            ->method('getConfig')
            ->with('deploy-mode')
            ->willReturn('developer');

        $this->deployMode->expects($this->once())
            ->method('enableMode')
            ->with('developer');

        $arguments = [
            'config' => $config,
            'deployMode' => $this->deployMode
        ];

        $this->action = $this->objectManager->getObject('Vaimo\Magento\Plugin\Action\InstallMagento', $arguments);
        $this->action->execute($this->installerData);
    }
}
