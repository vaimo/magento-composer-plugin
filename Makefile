TESTS_RUN=tests_run
COMPOSER_FILES=composer.json composer.lock
INSTALLED_FILE=$(TESTS_RUN)/vendor/composer/installed.json

.PHONY: test cleanup

$(INSTALLED_FILE): $(COMPOSER_FILES)
	@mkdir -p $(TESTS_RUN)
	@cp $(COMPOSER_FILES) $(TESTS_RUN)
	@sed -i.bak 's/"src\/"/"..\/src\/"/' $(TESTS_RUN)/composer.json
	@composer install --ignore-platform-reqs --working-dir=$(TESTS_RUN)

test: $(INSTALLED_FILE)
	./$(TESTS_RUN)/vendor/bin/phpunit --log-junit  build/junit.xml --coverage-clover build/clover.xml

cleanup:
	rm -rf $(TESTS_RUN)
	mkdir $(TESTS_RUN)
	rm -rf build
	mkdir build
	