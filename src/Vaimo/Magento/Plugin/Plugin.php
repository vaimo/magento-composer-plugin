<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Vaimo\Magento\Plugin;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\ConsoleIO;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use Vaimo\Magento\Plugin\Action\InstallMagento;
use Vaimo\Magento\Plugin\Installer\Data;
use Vaimo\Magento\Plugin\Db\Config as DbConfig;
use Composer\Plugin\PluginEvents;

class Plugin implements PluginInterface, EventSubscriberInterface
{
    /**
     * @var \Composer\Composer
     */
    protected $composer;

    /**
     * @var \Composer\IO\IOInterface
     */
    protected $io;

    /**
     * @var Magento
     */
    protected $magento;

    public function activate(Composer $composer, IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }

    public static function getSubscribedEvents()
    {
        return array(
            ScriptEvents::POST_INSTALL_CMD => array(
                array('onPostInstallCmd', 0),
            ),
            ScriptEvents::POST_UPDATE_CMD => array(
                array('onPostUpdateCmd', 0),
            ),
        );
    }

    public function onPostInstallCmd(Event $event)
    {
        $this->installMagento();
    }

    public function onPostUpdateCmd(Event $event)
    {
        if ($this->isMagentoInstalled()) {
            return;
        }

        $this->installMagento();
    }

    public function bootstrapMagento()
    {
        if ($this->magento) {
            return $this->magento;
        }

        $this->magento = Magento::bootstrap();
        
        return $this->magento;
    }

    private function isMagentoInstalled()
    {
        $this->bootstrapMagento();
        
        return $this->magento->isInstalled();
    }

    private function installMagento()
    {
        $config = Config::create($this->composer);

        if (!$config->getBooleanConfig('install-magento', true)) {
            if ($this->io->isVerbose()) {
                $this->io->write("<info>Skip Magento installation</info>");
            }
            
            return;
        }

        $this->bootstrapMagento();

        $dbConfig = DbConfig::create();

        $installerData = Data::create(
            $this->composer,
            $config,
            $dbConfig
        );

        $deployMode = new DeployMode(
            $this->magento,
            $this->io
        );

        $installAction = InstallMagento::create(
            $this->io,
            $config,
            $dbConfig,
            $this->magento,
            $deployMode
        );

        $installAction->execute($installerData);
    }


    public function deactivate(Composer $composer, IOInterface $io)
    {
        // TODO: Implement deactivate() method.
    }

    public function uninstall(Composer $composer, IOInterface $io)
    {
        // TODO: Implement uninstall() method.
    }
}
