<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Vaimo\Magento\Plugin;

use Composer\Composer;

class Config
{
    const EXTRA_SECTION_KEY = 'magento-composer-plugin';

    const ENV_KEY_PREFIX = 'MCP_';

    protected $env;

    protected $extra;

    public function __construct(array $env, array $extra = array())
    {
        $this->env = $env;
        $this->extra = $extra;
    }

    public static function create(Composer $composer)
    {
        $env = $_SERVER;
        $extra = $composer->getPackage()->getExtra();
        $extra = isset($extra[self::EXTRA_SECTION_KEY]) ? $extra[self::EXTRA_SECTION_KEY] : array();

        return new self(
            $env,
            $extra
        );
    }

    public function getConfig($key, $default = null)
    {
        $config = $this->getEnvConfig($key);

        if ($config !== null) {
            return $config;
        }

        if (isset($this->extra[$key])) {
            return $this->extra[$key];
        }

        return $default;
    }

    protected function getEnvConfig($key)
    {
        $key = self::ENV_KEY_PREFIX . strtoupper(str_replace('-', '_', $key));

        if (isset($this->env[$key])) {
            return $this->env[$key];
        }

        return null;
    }

    public function getBooleanConfig($key, $default = null)
    {
        $config = $this->getConfig($key, $default);

        if ($config === null) {
            return null;
        }

        return boolval($config);
    }
}