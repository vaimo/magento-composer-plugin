<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Vaimo\Magento\Plugin\Installer;

use Composer\Composer;
use Vaimo\Magento\Plugin\Config;
use Vaimo\Magento\Plugin\Host;
use Vaimo\Magento\Plugin\Db\Config as DbConfig;

class Data
{
    protected $packageName;

    protected $projectDir;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var DbConfig
     */
    protected $dbConfig;

    protected $host;

    public function __construct(
        $packageName,
        $projectDir,
        Config $config,
        DbConfig $dbConfig,
        Host $host)
    {
        $this->packageName = $packageName;
        $this->projectDir = $projectDir;
        $this->config = $config;
        $this->dbConfig = $dbConfig;
        $this->host = $host;
    }

    public static function create(Composer $composer, Config $config, DbConfig $dbConfig)
    {
        return new self(
            $composer->getPackage()->getName(),
            BP,
            $config,
            $dbConfig,
            new Host()
        );
    }

    public function getRequest()
    {
        $request = [
            'base-url' => $this->getBaseUrl(),
            'backend-frontname' => $this->getBackendFrontname(),
            'session-save' => $this->config->getConfig('session-save', 'files'),
            'db-host' => $this->getDbHost(),
            'db-name' => $this->getDbName(),
            'db-user' => $this->getDbUser(),
            'db-password' => $this->getDbPassword(),
            'db-model' => $this->config->getConfig('db-model', 'mysql4'),
            'db-engine' => $this->config->getConfig('db-engine', 'innodb'),
            'db-prefix' => $this->config->getConfig('db-prefix', ''),
            'db-ssl-key' => $this->config->getConfig('db-ssl-key', ''),
            'db-ssl-cert' => $this->config->getConfig('db-ssl-cert', ''),
            'db-ssl-ca' => $this->config->getConfig('db-ssl-ca', ''),
            'db-ssl-verify' => $this->config->getConfig('db-ssl-verify', false),
            'db-init-statements' => $this->config->getConfig('db-init-statements', 'SET NAMES utf8;'),
            'admin-firstname' => $this->config->getConfig('admin-firstname', 'Magento'),
            'admin-lastname' => $this->config->getConfig('admin-lastname', 'User'),
            'admin-email' => $this->config->getConfig('admin-email', 'user@example.com'),
            'admin-user' => $this->getAdminUser(),
            'admin-password' => $this->getAdminPassword(),
            'language' => $this->config->getConfig('language', 'en_US'),
            'currency' => $this->config->getConfig('currency', 'USD'),
            'timezone' => $this->config->getConfig('timezone', 'America/Chicago'),
            'use-rewrites' => $this->config->getBooleanConfig('use-rewrites', true),
            'cleanup-database' => $this->shouldCleanupDatabase()
        ];

        return $request;
    }

    public function getBaseUrl()
    {
        $fqdn = $this->config->getConfig('fqdn', $this->host->getFQDN());
        $basename = $this->getProjectBasename();

        return $this->config->getConfig('base-url', "http://{$fqdn}/{$basename}/");
    }

    public function getDbHost()
    {
        return $this->config->getConfig('db-host', $this->dbConfig->getHost());
    }

    public function getDbName()
    {
        $normalizedName = preg_replace('/[^a-z0-9]+/', '-', strtolower($this->getProjectBasename()));

        return $this->config->getConfig('db-name', $normalizedName);
    }

    public function getDbUser()
    {
        return $this->config->getConfig('db-user', $this->dbConfig->getUser());
    }

    public function getDbPassword()
    {
        return $this->config->getConfig('db-password', $this->dbConfig->getPassword());
    }

    private function getProjectBasename()
    {
        return basename($this->projectDir);
    }

    public function getBackendFrontname()
    {
        return $this->config->getConfig('backend-frontname', 'admin');
    }

    public function getAdminUrl()
    {
        return $this->getBaseUrl() . $this->getBackendFrontname();
    }

    public function getAdminUser()
    {
        return $this->config->getConfig('admin-user', 'admin');
    }

    public function getAdminPassword()
    {
        return $this->config->getConfig('admin-password', 'test123');
    }

    public function shouldCleanupDatabase()
    {
        return $this->config->getBooleanConfig('cleanup-database', false);
    }
}
