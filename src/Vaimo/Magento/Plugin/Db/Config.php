<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Vaimo\Magento\Plugin\Db;

class Config
{
    protected $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public static function create()
    {
        $config = parse_ini_file(getenv('HOME') . '/.my.cnf', true);
        return new self($config['client']);
    }

    public function getConfig()
    {
        return [
            'active' => true,
            'dbname' => 'mysql',
            'username' => $this->config['user'],
            'password' => $this->config['password'],
            'host' => isset($this->config['host']) ? $this->config['host'] : 'localhost'
        ];
    }

    public function getUser()
    {
        return $this->config['user'];
    }

    public function getPassword()
    {
        return $this->config['password'];
    }

    public function getHost()
    {
        return $this->config['host'] ?? 'localhost';
    }
}
