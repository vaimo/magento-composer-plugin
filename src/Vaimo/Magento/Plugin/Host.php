<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Vaimo\Magento\Plugin;

class Host
{
    public function getFQDN()
    {
        return trim(shell_exec('hostname -f'));
    }
}