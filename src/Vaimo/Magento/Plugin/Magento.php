<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Vaimo\Magento\Plugin;

use Magento\Framework\App\Bootstrap;
use Magento\Framework\ObjectManagerInterface;
use Magento\Setup\Model\ObjectManagerProvider;
use Laminas\ServiceManager\ServiceManager;

class Magento
{
    const APP_BOOTSTRAP_FILE = '/../../../../../../../app/bootstrap.php';

    /**
     * @var \Magento\Framework\App\Bootstrap
     */
    protected $bootstrap;

    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    public function __construct(Bootstrap $bootstrap, ServiceManager $serviceManager, ObjectManagerInterface $objectManager)
    {
        $this->bootstrap = $bootstrap;
        $this->serviceManager = $serviceManager;
        $this->objectManager = $objectManager;
    }

    public static function bootstrap()
    {
        $appBootstrapFile = __DIR__ . self::APP_BOOTSTRAP_FILE;

        if (!file_exists($appBootstrapFile)) {
            throw new \RuntimeException('Magento bootstrap file ' . self::APP_BOOTSTRAP_FILE . ' was not found');
        }

        require_once $appBootstrapFile;

        $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
        $configPath = require BP . '/setup/config/application.config.php';

        if (class_exists('\Magento\Setup\Application')) {
            $appWrapper = new \Magento\Setup\Application();
            $app = $appWrapper->bootstrap($configPath);
        } else {
            $app = \Laminas\Mvc\Application::init($configPath);
        }

        $objectManager = $bootstrap->getObjectManager();
        $omProvider = $app->getServiceManager()->get(ObjectManagerProvider::class);
        $omProvider->setObjectManager($objectManager);

        return new self(
            $bootstrap,
            $app->getServiceManager(),
            $bootstrap->getObjectManager()
        );
    }

    public function reload()
    {
        $this->bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
        $this->objectManager = $this->bootstrap->getObjectManager();

        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }

    public function isInstalled()
    {
        /** @var \Magento\Framework\App\DeploymentConfig $deploymentConfig */
        $deploymentConfig = $this->getObjectManager()->get('Magento\Framework\App\DeploymentConfig');
        return $deploymentConfig->isAvailable();
    }
}
