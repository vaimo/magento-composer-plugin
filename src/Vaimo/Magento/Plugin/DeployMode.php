<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Vaimo\Magento\Plugin;

use Composer\IO\IOInterface;
use \Magento\Deploy\Model\Mode;
use \Magento\Framework\App\State;


class DeployMode
{
    protected $magento;

    protected $io;

    /**
     * @var \Magento\Deploy\Model\Mode
     */
    protected $mode;

    public function __construct(Magento $magento, IOInterface $io, Mode $mode = null)
    {
        $this->magento = $magento;
        $this->io = $io;
        $this->mode = $mode;
    }

    private function initModeController()
    {
        if ($this->mode) {
            return;
        }

        $input = new \Symfony\Component\Console\Input\ArrayInput([]);
        $output = new \Symfony\Component\Console\Output\ConsoleOutput();

        $this->mode = $this->magento->getObjectManager()->create(
            'Magento\Deploy\Model\Mode',
            [
                'input' => $input,
                'output' => $output,
            ]
        );
    }

    private function getModeController()
    {
        $this->initModeController();
        return $this->mode;
    }

    public function enableMode($toMode)
    {
        switch($toMode) {
            case State::MODE_DEVELOPER:
                $this->getModeController()->enableDeveloperMode();
                $this->io->write('<info>Enabled developer mode</info>');
                break;
            case State::MODE_PRODUCTION:
                $this->getModeController()->enableProductionMode();
                $this->io->write('<info>Enabled production mode</info>');
                break;
        }
    }
}