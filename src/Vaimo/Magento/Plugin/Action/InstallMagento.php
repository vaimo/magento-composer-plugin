<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Vaimo\Magento\Plugin\Action;

use Composer\IO\IOInterface;
use Magento\Setup\Model\Installer;
use Vaimo\Magento\Plugin\Config;
use Vaimo\Magento\Plugin\ConsoleLogger;
use Vaimo\Magento\Plugin\Db;
use Vaimo\Magento\Plugin\Db\Config as DbConfig;
use Vaimo\Magento\Plugin\Installer\Data;
use Vaimo\Magento\Plugin\Magento;
use Vaimo\Magento\Plugin\DeployMode;

class InstallMagento
{
    protected $io;

    protected $magento;

    protected $installer;

    protected $db;

    protected $config;

    protected $deployMode;

    public function __construct(
        IOInterface $io,
        Magento $magento,
        Installer $installer,
        Db $db,
        Config $config,
        DeployMode $deployMode
    )
    {
        $this->io = $io;
        $this->magento = $magento;
        $this->installer = $installer;
        $this->db = $db;
        $this->config = $config;
        $this->deployMode = $deployMode;
    }

    public static function create(IOInterface $io, Config $config, DbConfig $dbConfig, Magento $magento, DeployMode $deployMode)
    {
        $installerFactory = $magento->getServiceManager()->get('Magento\Setup\Model\InstallerFactory');
        $installer = $installerFactory->create(new ConsoleLogger($io));

        $dbAdapter = $magento->getObjectManager()->create('\Magento\Framework\Model\ResourceModel\Type\Db\Pdo\Mysql', [
            'config' => $dbConfig->getConfig()
        ]);

        $db = new Db($dbAdapter->getConnection(
            $magento->getServiceManager()->get('Magento\Framework\DB\Logger\Quiet')
        ));

        return new self($io, $magento, $installer, $db, $config, $deployMode);
    }

    public function execute(Data $installerData)
    {
        if ($this->magento->isInstalled()) {
            $this->logVerboseInfo('Magento site already installed');
            return false;
        }

        if (!$installerData->shouldCleanupDatabase()) {
            $this->askReplaceDatabase($installerData);

            $this->logVerboseInfo("Create database {$installerData->getDbName()}");

            $this->db->createDatabase(
                $installerData->getDbName()
            );
        }

        $this->installer->install(
            $installerData->getRequest()
        );

        // Need to reload the Magento app after installation to be able to set deploy mode
        $this->magento->reload();

        $this->deployMode->enableMode(
            $this->config->getConfig('deploy-mode')
        );

        $this->outputInstallerInfo($installerData);

        return true;
    }

    private function logVerboseInfo($message)
    {
        if ($this->io->isVerbose()) {
            $this->io->write("<info>{$message}</info>");
        }
    }

    private function askReplaceDatabase(Data $installerData)
    {
        if (!$this->db->databaseExists($installerData->getDbName())) {
            return;
        }

        if (!$this->io->isInteractive()) {
            return;
        }

        $replaceDatabase = $this->io->askConfirmation(
            "A database named {$installerData->getDbName()} already exists, do you want to replace it? [y/N] ", false);

        if (!$replaceDatabase) {
            return;
        }

        $this->logVerboseInfo("Drop database {$installerData->getDbName()}");

        $this->db->dropDatabase(
            $installerData->getDbName()
        );
    }

    private function outputInstallerInfo(Data $installerData)
    {
        $this->io->write("<info>Main url {$installerData->getBaseUrl()}
Admin url {$installerData->getAdminUrl()}
User: {$installerData->getAdminUser()}
Password: {$installerData->getAdminPassword()}</info>");
    }
}