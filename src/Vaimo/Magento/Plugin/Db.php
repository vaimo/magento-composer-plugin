<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Vaimo\Magento\Plugin;

use Magento\Framework\DB\Adapter\AdapterInterface;


class Db
{
    protected $connection;

    public function __construct(AdapterInterface $connection)
    {
        $this->connection = $connection;
    }

    public function createDatabase($dbName)
    {
        $quotedDbName = $this->connection->quoteIdentifier($dbName);
        $this->connection->query("CREATE DATABASE IF NOT EXISTS {$quotedDbName}");
    }

    public function databaseExists($dbName)
    {
        $result = $this->connection->query(
            "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = ?", [
                $dbName
            ]
        );

        return $result->rowCount() == 1;
    }

    public function dropDatabase($dbName)
    {
        $quotedDbName = $this->connection->quoteIdentifier($dbName);
        $this->connection->query("DROP DATABASE {$quotedDbName}");
    }
}