<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Vaimo\Magento\Plugin;

use Composer\IO\IOInterface;
use Magento\Framework\Setup\LoggerInterface;

class ConsoleLogger implements LoggerInterface
{
    /**
     * @var \Composer\IO\IOInterface
     */
    protected $io;

    public function __construct(IOInterface $io)
    {
        $this->io = $io;
    }

    /**
     * Logs success message
     *
     * @param string $message
     * @return void
     */
    public function logSuccess($message)
    {
        $this->io->write('<info>' . $message . '</info>');
    }

    /**
     * Logs error message
     *
     * @param \Exception $e
     * @return void
     */
    public function logError(\Exception $e)
    {
        $this->io->writeError($e->getMessage());
    }

    /**
     * Logs a message
     *
     * @param string $message
     * @return void
     */
    public function log($message)
    {
        $this->io->write('<info>' . $message . '</info>');
    }

    /**
     * Logs a message in the current line
     *
     * @param string $message
     * @return void
     */
    public function logInline($message)
    {
        $this->io->write($message, false);
    }

    /**
     * Logs meta information
     *
     * @param string $message
     * @return void
     */
    public function logMeta($message)
    {
        $this->io->write($message);
    }
}
