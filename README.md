# vaimo/magento-composer-plugin

Implements common functionality that is useful when managing a Composer project that includes any edition 
of Magento 2.

## Features

* Automatically creates a database and installs Magento 2 after running composer install

## Supports

* Magento 2.0
* Magento 2.1
* Magento 2.2
* Magento 2.3
* Magento 2.4 (up to 2.4.6-p5 with most recent 3.0 version)

## Usage

Require vaimo/magento-composer-plugin in your Magento 2 project by calling:  

    composer require vaimo/magento-composer-plugin

### Example composer.json

```
#!json

    {
        ...

        "require": {
            "magento/product-community-edition": "~2.0",
            "vaimo/magento-composer-plugin": "~2.1"
        }

        ...
    }
    
```

## Magento admin user login credentials

* Username: admin
* Password: test123

## Note

The plugin will use the database credentials found in `~/.my.cnf` so I would advice you to not use the 
plugin to install production sites unless you know what you are doing.  

## Plugin events used

* post-install-cmd: Automatically creates a database and installs Magento 2.

## Configuration

The plugin can be configured by using environment variables and / or setting values in the `extra` 
section in composer.json

### Environment variables

All environment variables should be in uppercase, prefixed with `MCP_` and `-` replaced with `_`. Example:

    MCP_DB_NAME=project_database
    MCP_USE_REWRITES=0

### composer.json - extra section

To configure the plugin in a Composer project, specify config values in the extra section inside of 
the `magento-composer-plugin` section. Example:
    
```
#!json
    
    {
        "extra": {
            "magento-composer-plugin": {
                "db-name": "project_database"
                ...
            }
        }
    }
    
```

### Available configuration

(value) = default value

#### General configuration

* 'install-magento' (true). If Magento should be installed
* 'cleanup-database' (false). Cleanup the database before installation
* 'deploy-mode' (default). Set deploy mode after installation. Available modes are 'developer' and 'production'

Use Composer `--no-interaction / COMPOSER_NO_INTERACTION` if you want to disable interaction.

#### Installer data

* 'fqdn' (taken from hostname --fqdn)
* 'base-url'
* 'backend-frontname' (admin)
* 'session-save' (files)
* 'db-host' (localhost)
* 'db-name' (project directory)
* 'db-user' (random)
* 'db-password' (random)
* 'db-model' (mysql4)
* 'db-engine' (innodb)
* 'db-prefix' ('')
* 'db-init-statements' (SET NAMES utf8;)
* 'admin-firstname' (Magento)
* 'admin-lastname' (User)
* 'admin-email' (user@example.com)
* 'admin-user' (admin)
* 'admin-password' (test123)
* 'language' => (en_US)
* 'currency' => (USD)
* 'timezone' => (America/Chicago)
* 'use-rewrites' => (true)

## Development

### Test commands

To manually test included commands, the easiest way is to have the plugin installed in a project and use 
the `run-script` command in Composer. To test the `post-install-cmd`:

    composer run-script post-install-cmd

### Run tests

    make test
